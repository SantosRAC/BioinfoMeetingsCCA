# Bioinfo Meetings CCA

Este repositório contém os códigos e dados associados às atividades do grupo de estudos "Bioinfo Meetings no CCA"

## Pessoas

### Coordenação geral

 * Dr. Renato A. Corrêa dos Santos ([renatoacsantos@gmail.com](mailto:renatoacsantos@gmail.com))
 * Prof. Dr. Renato Nallin Montagnolli (UFSCar - Campus Araras)
 * Prof. Dra. Elaine Silva Dias (Universidade Federal Rural da Amazônia - Campus Capanema)

### Coordenação de atividades

 * Dr. Renato A. Corrêa dos Santos

### Membros ativos (maio/2022)

 * 

### Nossos colaboradores nas redes sociais e divulgação científica

 * Luiza Leme
 * Renato Cardoso
 * Marcos Godoy
 * Rafaella Volpi

#### Nossos colaboradores em atividades de ensino

 * Raniere Gaia Costa da Silva
 * 


## Atividades (no repositório)

 * Code clubs de programação em Python
 
 - Nível Iniciante (Jupyter e Colab)
 - Nível Avançado (Problemas do Rosalind)

 ## Contato

  * E-mail: [bioinfo.meetings.cca@gmail.com](mailto:bioinfo.meetings.cca@gmail.com)
  * [Instagram](https://www.instagram.com/bioinfomcca/)
  * [Twitter](https://twitter.com/BioinfoMeetsCCA)

