# Caderno de teste - Análises de genomas de Cronobacter com Pacbio (TGS)

Autores:
 * Grupo de Estudos Bioinfo Meetings no CCA

## Algumas etapas de geração dos dados

As análises do grupo de estudos com _Cronobacter_ serão feitas com base nos dados de sequenciamento gerados por [Moine et al. (2016)](https://doi.org/10.1128/genomeA.00142-16)

Depois da extração de DNA:
 * Biblioteca de 20-kb com protocolo PacBio e seleção por tamanho com Blupippin
 * Sequenciamento realizado em plataforma PacBio RSII - P4/C2
 * Três ou quatro células SMRT por linhagem c/ 180-min de protocolo

## Análise inicial dos dados de sequenciamento

Alguns testes iniciais serão realizados com dados de _C. condimenti_ (SRA: [SRR2154341](https://www.ncbi.nlm.nih.gov/sra/SRR2154341)). Algumas informações no NCBI:

 * Name: Pacbio
 * Instrument: PacBio RS II
 * Strategy: WGS
 * Source: GENOMIC
 * Selection: unspecified
 * Layout: SINGLE

### Baixando os dados com grabseqs e SRA Toolkit

O programa [grabseqs](https://github.com/louiejtaylor/grabseqs) pode ser usado para fazer o download de reads do NCBI facilmente, usando a linha de comando.

Instalando o programa com pip (Python):

```{bash}
$ pip3 install grabseqs
```

Verificando a versão:

```{bash}
$ grabseqs -v
```

Resultado da consulta:

```{bash}
grabseqs 0.7.0
```

Antes de rodar o grabseqs é preciso instalar o SRA Toolkit.
Primeiramente, baixei o SRA Toolkit [aqui](https://github.com/ncbi/sra-tools).


Após inclusão do programa na variável PATH (arquivo `.bashrc`), rodei o comando abaixo para configurar o SRA Toolkit:

```{bash}
$ vdb-config --interactive
```

Após ter certeza que tudo estava funcionando direitinho, rodei o comando abaixo para fazer o download de reads para as corridas:
 * SRR2154341
 * SRR3112550

```{bash}
$ grabseqs sra SRR2154341 SRR3112550
```

### Entendendo os arquivos de sequenciamento

Arquivos gerados no sequenciamento:
 * h5
 * ?


## Montando o genoma

No grupo de estudos, vamos testar diferentes montadores de genoma para leituras longas de sequenciamento.

Uma lista inicial de montagens de genomas pode ser obtido de uma \
[referência recente](https://doi.org/10.1093/bib/bbx147) sobre \
comparação de montadores usando SMRT reads, unicamente (montagens não híbridas):
 * Canu
 * Hierarchical Genome Assembly Process
 * PBcR
 * FALCON
 * HINGE
 * Miniasm
 * SMARTdenovo
 * ABruijn
 * Wtdbg


### Canu

#### Instalando o Canu

Um dos montadores de genoma especializados em leituras longas é o [Canu](https://github.com/marbl/canu.git).


Clonando o repositório:

```{bash}
git clone https://github.com/marbl/canu.git
```

Instalando (com 4 threads):

```{bash}
$ cd canu/src/
$ make -j 4
```

#### Testando o Canu

Testando uma versão de desenvolvimento:

```{bash}
$ ./canu -version
canu snapshot v2.3-development +69 changes (r10340 9b4be3cc7df8a170f7533524bd00e04e0a80ad99)
```

Rodando o montador de acordo com os exemplos da página de quick start:

```{bash}
$ canu -p cronobacter -d cronobacter_pacbio genomeSize=4.4m \
 -pacbio SRR2154341.fastq.gz SRR3112550.fastq.gz
```


## Referências

 * [Motyka-Pomagruk](https://doi.org/10.1007/978-1-0716-1099-2_1), Agata, et al. "PacBio-based protocol for bacterial genome assembly." Bacterial Pangenomics. Humana, New York, NY, 2021. 3-14.
 * [Rhoads](https://doi.org/10.1016/j.gpb.2015.08.002), Anthony, and Kin Fai Au. "PacBio sequencing and its applications." Genomics, proteomics & bioinformatics 13.5 (2015): 278-289.
 * [Taylor](https://doi.org/10.1093/bioinformatics/btaa167), Louis J., Arwa Abbas, and Frederic D. Bushman. "grabseqs: simple downloading of reads and metadata from multiple next-generation sequencing data repositories." Bioinformatics 36.11 (2020): 3607-3609.
 * [Koren](http://doi.org/10.1101/gr.215087.116), Sergey, et al. "Canu: scalable and accurate long-read assembly via adaptive k-mer weighting and repeat separation." Genome research 27.5 (2017): 722-736.
 * [Jayakumar](https://doi.org/10.1093/bib/bbx147), Vasanthan, and Yasubumi Sakakibara. "Comprehensive evaluation of non-hybrid genome assembly tools for third-generation PacBio long-read sequence data." Briefings in bioinformatics 20.3 (2019): 866-876.
